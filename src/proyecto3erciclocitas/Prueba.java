package proyecto3erciclocitas;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
abstract class Clinica {
    Archivo Medico = new Archivo();
    Archivo Pacientes = new Archivo();
    private double sueldo;
    public abstract ArrayList<Double> sueldo();
}
public class Prueba {
    static File MedicoArchivo = new File("Medicos.txt");
    static File CitasTotalesArchivo = new File("TotalCitas.txt");
    static Scanner teclado = new Scanner(System.in);
    static Paciente objPaciente = new Paciente();
    static Metodos objMetodos = new Metodos();
    static Prueba objPrueba = new Prueba();
    static PersonalMedico objPersonalMedico = new PersonalMedico();
    static Secretaria objSecretaria = new Secretaria();
    static String Area = "";
    //public static ArrayList <personalMedico> personal = new ArrayList <personalMedico> ();
    public static void main(String[] args) {
        objPrueba.MenuPrincipal();
    }
    public void MenuPrincipal() {
        int opc;
        do {
            System.out.println("▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ CLINICA DE ESPECIALIDADES ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀");
            System.out.println("DONDE DESEA ENTRAR");
            System.out.println("1. Medicos Disponibles");
            System.out.println("2. Secretaria");
            System.out.println("3. Salir");
            System.out.print(">>>>>>>>");
            opc = teclado.nextInt();
            // opc =  Integer.parseInt(teclado.next());
            if (opc == 3) {
                break;
            }
            switch (opc) {
                case 1:
                    objMetodos.MostrarMedicos();
                    MenuPrincipal();
                break;
                case 2:
                    objPrueba.MenuSecretaria();
            }
        } while (opc == 3);
    }

    public void Medicos() {
        int opc;
        System.out.println("ESCOGA UNA OPCION");
        System.out.println("1. Mostrar Medicos");
        System.out.println("2. Regresar al menú Principal");
        System.out.println(">>>>>>>>>");
        opc = teclado.nextInt();
        switch (opc) {
            case 1:
                objMetodos.MostrarMedicos();
                Medicos();
                break;
            case 2:
                objPrueba.MenuPrincipal();
                break;
        }
    }

    public void MenuSecretaria() {
        String Password;
        System.out.print("►►►►►►►►►INGRESE CONTRASEÑA DE ACCESO: ");
        Password = teclado.next();
        if (Password.equals("123456")) {
            int opc;
            do {
                System.out.println("ESCOGA UNA OPCION");
                System.out.println("1. PACIENTES");
                System.out.println("2. MEDICOS");
                System.out.println("3. CALCULAR SUELDOS, ESPECILIDADES MAS PEDIDAS");
                System.out.println("4. REGRESAR AL MENÚ PRINCIPAL");
                System.out.println("5. Salir");
                System.out.print(">>>>>>>>>");
                opc = teclado.nextInt();
                if (opc == 5) {
                    break;
                }
                switch (opc) {
                    case 1:
                        MenuPacientes();
                    case 2:
                        MenuMedicos();
                    case 3:
                        objPersonalMedico.sueldo();
                        objSecretaria.toString();
                    case 4:
                        MenuPrincipal();
                    case 5:
                        break;
                }
            } while (opc != 5);

        } else {
            System.out.println("¡¡¡CONTRASEÑA INCORRECTA!!! VUELVA A INTENTARLO");
            MenuSecretaria();
        }

    }

    public void MenuMedicos() {
        int opc;
        PersonalMedico objPersonalMedico = new PersonalMedico();
        Secretaria objSecretaria = new Secretaria();
        System.out.println("ESCOGA UNA OPCION");
        System.out.println("1. Agregar un nuevo médico");
        System.out.println("2. Buscar médico");
        System.out.println("3. Eliminar Medico");
        System.out.println("4. Mostar médicos disponibles");
        System.out.println("5. Calcular Sueldos y Especilidad mas pedida");
        System.out.println("6. Regresar");
        System.out.print(">>>>>>>>>");
        opc = teclado.nextInt();
        switch (opc) {
            case 1:
                objMetodos.AgregarMedico();
                MenuMedicos();
                break;
            case 2:
                objMetodos.BuscarMedico();
                MenuMedicos();
                break;
            case 3:
                objMetodos.EliminarMedico();
                MenuMedicos();
                break;
            case 4:
                objMetodos.MostrarMedicos();
                MenuMedicos();
                break;
            case 5:
                objPersonalMedico.sueldo();
                break;
            case 6:
                objPrueba.MenuSecretaria();
                break;
        }
    }

    public void MenuPacientes() {
        int opc;
        System.out.println("ESCOGA UNA OPCION");
        System.out.println("1. Agendar una cita");
        System.out.println("2. Eliminar Cita");
        System.out.println("3. Buscar cita");
        System.out.println("4. Mostar citas totales");
        System.out.println("5. Regresar");
        System.out.print(">>>>>>>>>");
        opc = teclado.nextInt();
        switch (opc) {
            case 1:
                MenuAgendar();
                break;
            case 2:
                 objMetodos.EliminarPaciente();
                 MenuPacientes();
            case 3:
                objMetodos.BuscarPaciente();
                MenuPacientes();
            case 4:
                objMetodos.MostrarPacientes(); 
                MenuPacientes();
            case 5:
                objPrueba.MenuSecretaria();
        }
    }
    public void MenuAgendar() {
        int opc;
        System.out.println("EN QUE ESPECIALIDAD DESEA AGREGAR LA CITA");
        System.out.println("1. ODONTOLOGÍA");
        System.out.println("2. DERMATOLOGÍA");
        System.out.println("3. GINECOLOGÍA");
        System.out.println("4. OFTALMOLOGÍA");
        System.out.println("5. Regresar ");
        System.out.print(">>>>>>>>>");
        opc = teclado.nextInt();
        switch (opc) {
            case 1:
                objMetodos.AgregarPaciente(objPaciente, CitasTotalesArchivo, "Odontologia");
                break;
            case 2:
                objMetodos.AgregarPaciente(objPaciente, CitasTotalesArchivo, "Dermatologia");
                break;
            case 3:
                objMetodos.AgregarPaciente(objPaciente, CitasTotalesArchivo, "Ginecologia");
                break;
            case 4:
                objMetodos.AgregarPaciente(objPaciente, CitasTotalesArchivo, "Oftalmologia");
                break;
            case 5: 
                MenuPacientes();
                break;
        }
    }
}
