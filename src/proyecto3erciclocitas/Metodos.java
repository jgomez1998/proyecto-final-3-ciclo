package proyecto3erciclocitas;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import static proyecto3erciclocitas.Prueba.*;
public class Metodos {
    Scanner teclado = new Scanner(System.in);
    PersonalMedico personalMedico = new PersonalMedico();
    Prueba objPrueba = new Prueba();
    static Paciente objPaciente = new Paciente();
    static Archivo objArchivo = new Archivo();
    public void AgregarMedico() {
        PersonalMedico objPersonalMedico = new PersonalMedico();
        objArchivo.comprobarArchivo(MedicoArchivo);
        System.out.println("▀▀▀▀▀▀▀▀▀▀▀INGRESE DATOS DEL MÉDICO▀▀▀▀▀▀▀▀▀▀▀");
        System.out.print("Nombres:\t\t ");objPersonalMedico.setNOMBRE(teclado.nextLine());
        System.out.print("Telefono:\t\t ");objPersonalMedico.setTELEFONO(teclado.nextLine());
        System.out.print("Nº Cédula:\t\t ");objPersonalMedico.setCEDULA(teclado.nextLine());
        System.out.print("Especialidad:\t\t ");objPersonalMedico.setESPECIALIDAD(teclado.nextLine());
        System.out.print("Grado Académico:\t ");objPersonalMedico.setGRADOACADEMICO(teclado.nextLine());
        System.out.println("■■■■■■■■■■MEDICO AGREGADO CON EXITO■■■■■■■■■■");
        objArchivo.AgregarMedico(objPersonalMedico, MedicoArchivo);
        objPrueba.Medicos();
    }
    public void BuscarMedico() {
        objArchivo.BuscarMedico(personalMedico, MedicoArchivo);
    }
    public void EliminarMedico() {
        objArchivo.EliminarMedico(personalMedico, MedicoArchivo);
    }
    public void MostrarMedicos() {
        System.out.println("▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ MEDICOS DISPONIBLES ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ");
        objArchivo.MostrarMedicos(MedicoArchivo);
        System.out.println("▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ FIN DE RESULTADOS ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀");
    }
    /*public ArrayList<Double> SueldoMedicos(){
        return objArchivo.SueldoMedicos(CitasTotalesArchivo);
        
    }*/
    public void AgregarPaciente(Paciente objPaciente, File Archivo,String Area){
        objArchivo.comprobarArchivo(Archivo);
        AgregarPaciente(objPaciente);
        objArchivo.AgregarPaciente(objPaciente,  Archivo,Area);
        objPrueba.MenuPacientes();
    }
    public void BuscarPaciente(){
        objArchivo.BuscarPaciente(objPaciente, CitasTotalesArchivo);
    }
    public void EliminarPaciente(){
        objArchivo.EliminarPaciente(objPaciente, CitasTotalesArchivo);
    }
    public void MostrarPacientes() {
        objArchivo.MostrarPacientes(CitasTotalesArchivo);
    }
    public void AgregarPaciente(Paciente objPaciente) {  
        System.out.println("▀▀▀▀▀▀▀▀▀▀▀INGRESE DATOS DEL PACIENTE▀▀▀▀▀▀▀▀▀▀▀");
        System.out.print("Nombre: "); objPaciente.setNOMBRE(teclado.nextLine());
        System.out.print("Teléfono: "); objPaciente.setTELEFONO(teclado.nextLine());
        System.out.print("Nº Cédula: "); objPaciente.setCEDULA(teclado.nextLine());
        System.out.print("Dirección: "); objPaciente.setDIRECCION(teclado.nextLine());
        System.out.print("Fecha: "); objPaciente.setFECHA(teclado.nextLine());
        System.out.print("Hora: "); objPaciente.setHORA(teclado.nextLine());
    }
}
