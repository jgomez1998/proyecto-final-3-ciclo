package proyecto3erciclocitas;
public class Paciente {
    public Paciente() {
    }
    //Variables para informacion de la persona
    public String NOMBRE;
    public String TELEFONO;
    public String CEDULA;    
    public String DIRECCION;
    //VARIABLES SOBRE INFORMACION DE LA CITA
    public String FECHA;
    public String HORA;
    public String AREA;
    public Paciente(String NOMBRE, String TELEFONO, String CEDULA, String DIRECCION, String FECHA, String HORA, String AREA ) {
        this.NOMBRE = NOMBRE;
        this.TELEFONO = TELEFONO;
        this.CEDULA = CEDULA;
        this.DIRECCION = DIRECCION;
        this.FECHA = FECHA;
        this.HORA = HORA;
        this.AREA = AREA;
    }
    public String getNOMBRE() {
        return NOMBRE;
    }
    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }
    public String getTELEFONO() {
        return TELEFONO;
    }
    public void setTELEFONO(String TELEFONO) {
        this.TELEFONO = TELEFONO;
    }
    public String getCEDULA() {
        return CEDULA;
    }
    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }
    public String getDIRECCION() {
        return DIRECCION;
    }
    public void setDIRECCION(String DIRECCION) {
        this.DIRECCION = DIRECCION;
    }
    public String getFECHA() {
        return FECHA;
    }
    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
    public String getHORA() {
        return HORA;
    }
    public void setHORA(String HORA) {
        this.HORA = HORA;
    } 
}

