package proyecto3erciclocitas;

import java.util.*;//para el escaner lectura y entrada en consola
import java.io.*;//Para trabajar los archivos, define distintos flujos de datos
import java.util.Scanner;

public class Archivo {

    static File CitasTotalesArchivo = new File("TotalCitas.txt");
    static String aux;

    public void comprobarArchivo(File Archivo) {
        try {//Para el manejo de archivos
            if (Archivo.exists()) {//Si el archivo existe hay como llenar el archivo
                System.out.println("» » » » » » » » PUEDE TRABAJAR EN EL ARCHIVO « « « « « « « «");
            } else {
                Archivo.createNewFile();
            }
        } catch (IOException e) {// Si hay algun problema en el manejo de archivos
            System.out.println("Error" + e.getMessage());
            System.out.println("NO SE PUEDE TRABAJAR EN EL ARCHIVO\n");
        }
    }

    public void AgregarMedico(PersonalMedico personalMedico, File Archivo) {
        //Scanner teclado = new Scanner(System.in);
        String sepCampo = "|";//Separador de campo
        int tamRegistro = 0; // Aqui calculamos el tamaño del registro
        //int motivo;
        try {
            //Bufferedwiter para escribir sobre ficheros 
            //True por si tenemos datos en el archivo que siga escribiendo
            //Fescribe funcion para escribir en el archivo, es una instancia(objeto) de la clase
            //Instancia una clase, es crear un objeto dentro de otra clase
            //FileOutputStream sirve para escribir en el archivo datos orientados a caracteres
            BufferedWriter Fescribe = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Archivo, true)));
            //Prueba objClinica = new Prueba();
            // Prueba.AgregarMedico();
            //Se mide el tamaño del registro
            //length para obterner la longitud del campo y almacenar en la variable tamRegistro
            tamRegistro = sepCampo.length() + personalMedico.getNOMBRE().length() + sepCampo.length() + personalMedico.getTELEFONO().length()
                    + sepCampo.length() + personalMedico.getCEDULA().length() + sepCampo.length() + personalMedico.getESPECIALIDAD().length()
                    + sepCampo.length() + personalMedico.getGRADOACADEMICO().length();
            //Metodo para escribir en el archivo
            Fescribe.write(sepCampo + tamRegistro + sepCampo + personalMedico.getNOMBRE() + sepCampo + personalMedico.getTELEFONO() + sepCampo
                    + personalMedico.getCEDULA() + sepCampo + personalMedico.getESPECIALIDAD() + sepCampo + personalMedico.getGRADOACADEMICO()
                    + sepCampo);
            //Metodo para cerrar el archivo
            Fescribe.close();//Cerramos el fichero

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void BuscarMedico(PersonalMedico personalMedico, File Archivo) {
        Scanner teclado = new Scanner(System.in);
        String buscar, leer;
        boolean encontrado = true;
        try {//Se ingresa las sentencias para validar si tiene un error de ejecucion
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(Archivo));//Para almacenar la lectura del archivo
            //File reader es para leer ficheros de texto
            System.out.print("► ► ► ► ► ► INGRESE Nº DE CÉDULA: ");
            buscar = teclado.nextLine();
            while ((leer = br.readLine()) != null) {//Para que lea el archivo hasta que este no contenga nada y almacenaremos
                // lo que tenga el archivo en la variable leer 
                StringTokenizer st = new StringTokenizer(leer, "|");//Para separar los campos del registro 
                if (leer.contains(buscar)) {//Nos ayuda a comprobar lo que estamos buscando en la base de datos  
                    String cedula = null;
                    while (cedula != buscar && encontrado) {//Mientras cedula es diferente de BuscarMedico seguira buscando 
                        String tamregistro = st.nextToken();
                        String nombre = st.nextToken();
                        String telefono = st.nextToken();
                        cedula = st.nextToken();
                        String especialidad = st.nextToken();
                        String gradoacademico = st.nextToken();
                        if (cedula.equals(buscar)) {
                            encontrado = false;//Para ya no seguir almacenando mas datos en los tokens 
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » RESULTADO DE LA BÚSQUEDA « « « « « «");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La especialidad es:\t " + especialidad);
                            System.out.println("Su grado académico es:\t " + gradoacademico);
                            System.out.println("» » » » » » » » Busqueda Finalizada « « « « « « « «");
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("El resultado de la busqueda " + buscar + " no existe");
                    //Si no encuentra lo que estamos buscando nos imprimi ese mensaje y nos vuelve a mostrar el menuBuscarRegistro()
                }
                br.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());//muestra el mensaje del tipo de error que tiene sin parar el programa
        }
    }

    public void EliminarMedico(PersonalMedico personalMedico, File Archivo) {
        Scanner sc = new Scanner(System.in);
        String linea, buscar;
        String sepreg = "|";
        Vector lineasAcopiar = new Vector();//Clase Vector para almacenar lineas a copiar
        //La variable BuscarMedico va a recoger el numero de cedula para BuscarMedico datos relacionados con ese numero de cedula
        System.out.print("► ► ► ► ►INGRESE NUMERO DE CEDULA DE MEDICO A ELIMINAR ");
        buscar = sc.nextLine();
        try {
            //FileReader fr = new FileReader(ArchivoTXT);//Para leer el archivo
            BufferedReader br = new BufferedReader(new FileReader(Archivo));
            while (br.ready()) {//Si el buffer no esta vacio devuelve true
                linea = br.readLine();//Almacena todo lo que lee en el archivo en esta variable 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                if (linea.contains(buscar)) {//devulce un vlaor booleano
                    while (mistokens.hasMoreTokens()) {//comprueba si hay mas tokens por recorrer
                        String tamRegistro = mistokens.nextToken();
                        String nombre = mistokens.nextToken();
                        String telefono = mistokens.nextToken();
                        String cedula = mistokens.nextToken();
                        String especialidad = mistokens.nextToken();
                        String gradoAcademico = mistokens.nextToken();
                        //Aqui vamos almacenando los registro que leyo antes
                        String registros = sepreg + tamRegistro + sepreg + nombre + sepreg + telefono + sepreg + cedula
                                + sepreg + especialidad + sepreg + gradoAcademico;
                        if (!cedula.equals(buscar)) {
                            //Si la cedula es diferente o = a lo que estamos buscando lo vamos descartando del vector 
                            lineasAcopiar.add(registros);
                        } else {
                            //Imprimira los datos del paciente del cual se elimine su registro
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » EL REGISTRO ELIMINADO ES EL SIGUIENTE");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La especialidad es:\t " + especialidad);
                            System.out.println("Su Grado Académico es:\t " + gradoAcademico);
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("EL PACIENTE CON EL NUMERO DE CEDULA " + buscar + " NO EXISTE");
                }
                br.close();
                //SE RECORRE EL VECTOR Y SE GUARDAN LAS LINEAS EN EL FICHERO
                if (linea.contains(buscar)) {
                    BufferedWriter bw = new BufferedWriter(new FileWriter(Archivo));
                    for (int i = 0; i < lineasAcopiar.size(); i++) {//Recorrer el vextor
                        linea = (String) lineasAcopiar.elementAt(i);//Se guarda en linea cada string del vector
                        bw.write(linea);//Se escribe la linea en el fichero
                    }
                    bw.close();
                    System.out.println("EL REGISTRO CON NUMERO DE CEDULA " + buscar + " HA SIDO ELIMINADO");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void MostrarMedicos(File Archivo) {
        try {//No tiene parametros porque no necesita informacion previa par apoder continuar y mostrar los registros
            String linea;
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(Archivo));
            while ((linea = br.readLine()) != null) {
                //StringTokenizer ayuda a dividir strigns en base a otro string"caracter" o delimitador 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                while (mistokens.hasMoreTokens()) {//Si depsues de almacenar datos siguen habiendo mas los sigue almacenando
                    String tamRegistro = mistokens.nextToken();
                    //Va leyendo todos los datos
                    String NOMBRE = mistokens.nextToken();
                    String TELEFONO = mistokens.nextToken();
                    String CEDULA = mistokens.nextToken();
                    String ESPECIALIDAD = mistokens.nextToken();
                    String GRADOACADEMICO = mistokens.nextToken();
                    //Imprime los datos encontrados en el Archivo de texto de las citas agendadas
                    System.out.println("El nombre es:\t\t" + NOMBRE);
                    System.out.println("El teléfono es:\t\t" + TELEFONO);
                    System.out.println("La cédula es:\t\t" + CEDULA);
                    System.out.println("La dirección es:\t" + ESPECIALIDAD);
                    System.out.println("La fecha es:\t\t" + GRADOACADEMICO);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void AgregarPaciente(Paciente objPaciente, File Archivo, String Area) {
        String sepCampo = "|";
        int tamRegistro = 0;
        try {
            BufferedWriter Fescribe = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Archivo, true)));
            tamRegistro = sepCampo.length() + objPaciente.getNOMBRE().length() + sepCampo.length() + objPaciente.getTELEFONO().length()
                    + sepCampo.length() + objPaciente.getCEDULA().length() + sepCampo.length() + objPaciente.getDIRECCION().length()
                    + sepCampo.length() + objPaciente.getFECHA().length() + sepCampo.length() + objPaciente.getHORA().length()
                    + sepCampo.length() + Area.length();
            //Metodo para escribir en el archivo
            Fescribe.write(sepCampo + tamRegistro + sepCampo + objPaciente.getNOMBRE() + sepCampo + objPaciente.getTELEFONO() + sepCampo
                    + objPaciente.getCEDULA() + sepCampo + objPaciente.getDIRECCION() + sepCampo + objPaciente.getFECHA() + sepCampo
                    + objPaciente.getHORA() + sepCampo + Area);
            //Metodo para cerrar el archivo
            Fescribe.close();//Cerramos el fichero
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<Double> SueldoMedicos() {
        ArrayList<Double> sueldo = new ArrayList<>();
        double sueldoBase = 50;
        //  int Orden [] = new int [4];
        int auxOdontologia = 0, auxDermatologia = 0, auxGinecologia = 0, auxOftalmologia = 0;
        double sueldoOdontologo = 0, sueldoDermatologo = 0, sueldoGinecologo = 0, sueldoOftalmologo = 0;
        final String[] ESPECIALIDADES = {"ODONTOLOGA", "GINECOLOGA", "OFTALMOLOGO", "DERMATOLOGO"};
        try {//No tiene parametros porque no necesita informacion previa par apoder continuar y mostrar los registros
            String linea;

            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(CitasTotalesArchivo));
            while ((linea = br.readLine()) != null) {

                //StringTokenizer ayuda a dividir strigns en base a otro string"caracter" o delimitador 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");

                while (mistokens.hasMoreTokens()) {//Si depsues de almacenar datos siguen habiendo mas los sigue almacenando

                    String tamRegistro = mistokens.nextToken();
                    //Va leyendo todos los datos
                    String NOMBRE = mistokens.nextToken();
                    String TELEFONO = mistokens.nextToken();
                    String CEDULA = mistokens.nextToken();
                    String DIRECCION = mistokens.nextToken();
                    String FECHA = mistokens.nextToken();
                    String HORA = mistokens.nextToken();
                    String AREA = mistokens.nextToken();

                    if (AREA.equals("Odontologia")) {
                        auxOdontologia += 1;
                    }

                    if (AREA.equals("Dermatologia")) {
                        auxDermatologia += 1;
                    }
                    if (AREA.equals("Oftalmologia")) {
                        auxOftalmologia += 1;
                    }
                    if (AREA.equals("Ginecologia")) {
                        auxGinecologia += 1;
                    }
                }

            }
            int Orden[] = {auxOdontologia, auxDermatologia, auxGinecologia, auxOftalmologia};
            sueldoOdontologo = (auxOdontologia * 5) + sueldoBase;
            sueldoGinecologo = (auxGinecologia * 6) + sueldoBase;
            sueldoOftalmologo = (auxOftalmologia * 5) + sueldoBase;
            sueldoDermatologo = (auxDermatologia * 8) + sueldoBase;
            sueldo.add(sueldoOdontologo);
            sueldo.add(sueldoGinecologo);
            sueldo.add(sueldoOftalmologo);
            sueldo.add(sueldoDermatologo);
            for (int i = 0; i < Orden.length - 1; i++) {
                for (int j = 0; j < Orden.length - 1; j++) {
                    if (Orden[j] < Orden[j + 1]) {
                        int tmp = Orden[j + 1];
                        Orden[j + 1] = Orden[j];
                        Orden[j] = tmp;
                    }
                }
            }
            System.out.println("▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ESPECIALIDADES CON MAS PACIENTES ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀");
            for (int k = 0; k < Orden.length; k++) {
                System.out.print(ESPECIALIDADES[k]+" CON: "+Orden[k] + " CITAS\n");
            }
            return sueldo;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return sueldo;
        }
    }

    public void BuscarPaciente(Paciente objPaciente, File Archivo) {
        Scanner teclado = new Scanner(System.in);
        String buscar, leer;
        boolean encontrado = true;
        try {//Se ingresa las sentencias para validar si tiene un error de ejecucion
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(Archivo));//Para almacenar la lectura del archivo
            //File reader es para leer ficheros de texto
            System.out.print("► ► ► ► ► ►INGRESE Nº DE CÉDULA: ");
            buscar = teclado.nextLine();
            while ((leer = br.readLine()) != null) {//Para que lea el archivo hasta que este no contenga nada y almacenaremos
                // lo que tenga el archivo en la variable leer 
                StringTokenizer st = new StringTokenizer(leer, "|");//Para separar los campos del registro 
                if (leer.contains(buscar)) {//Nos ayuda a comprobar lo que estamos buscando en la base de datos  
                    String cedula = null;
                    while (cedula != buscar && encontrado) {//Mientras cedula es diferente de BuscarMedico seguira buscando 
                        String tamregistro = st.nextToken();
                        String nombre = st.nextToken();
                        String telefono = st.nextToken();
                        cedula = st.nextToken();
                        String direccion = st.nextToken();
                        String fecha = st.nextToken();
                        String hora = st.nextToken();
                        if (cedula.equals(buscar)) {
                            encontrado = false;//Para ya no seguir almacenando mas datos en los tokens 
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » RESULTADO DE LA BÚSQUEDA « « « « « «");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La dirección es:\t " + direccion);
                            System.out.println("La fecha de la cita es:\t " + fecha);
                            System.out.println("La hora de la cita es:\t " + fecha);
                            System.out.println("» » » » » » » » Busqueda Finalizada « « « « « « « «");
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("El resultado de la busqueda " + buscar + " no existe");
                    //Si no encuentra lo que estamos buscando nos imprimi ese mensaje y nos vuelve a mostrar el menuBuscarRegistro()
                }
                br.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());//muestra el mensaje del tipo de error que tiene sin parar el programa
        }
    }

    public void EliminarPaciente(Paciente objPaciente, File Archivo) {
        Scanner sc = new Scanner(System.in);
        String linea, buscar;
        String sepreg = "|";
        Vector lineasAcopiar = new Vector();//Clase Vector para almacenar lineas a copiar
        //La variable BuscarMedico va a recoger el numero de cedula para BuscarMedico datos relacionados con ese numero de cedula
        System.out.print("INGRESE NUMERO DE CEDULA DE PACIENTE A ELIMINAR ");
        buscar = sc.nextLine();
        try {
            //FileReader fr = new FileReader(ArchivoTXT);//Para leer el archivo
            BufferedReader br = new BufferedReader(new FileReader(Archivo));
            while (br.ready()) {//Si el buffer no esta vacio devuelve true
                linea = br.readLine();//Almacena todo lo que lee en el archivo en esta variable 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                if (linea.contains(buscar)) {//devulce un vlaor booleano
                    while (mistokens.hasMoreTokens()) {//comprueba si hay mas tokens por recorrer
                        String tamRegistro = mistokens.nextToken();
                        String nombre = mistokens.nextToken();
                        String telefono = mistokens.nextToken();
                        String cedula = mistokens.nextToken();
                        String direccion = mistokens.nextToken();
                        String fecha = mistokens.nextToken();
                        String hora = mistokens.nextToken();
                        String area = mistokens.nextToken();
                        //Aqui vamos almacenando los registro que leyo antes
                        String registros = sepreg + tamRegistro + sepreg + nombre + sepreg + telefono + sepreg + cedula
                                + sepreg + direccion + sepreg + fecha + sepreg + hora + sepreg + area;
                        if (!cedula.equals(buscar)) {
                            //Si la cedula es diferente o = a lo que estamos buscando lo vamos descartando del vector 
                            lineasAcopiar.add(registros);
                        } else {
                            //Imprimira los datos del paciente del cual se elimine su registro
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » EL REGISTRO ELIMINADO ES EL SIGUIENTE");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La direccion es:\t " + direccion);
                            System.out.println("La fecha de la cita:\t " + fecha);
                            System.out.println("La hora de la cita:\t " + hora);
                            System.out.println("El area de la cita:\t " + area);
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("EL PACIENTE CON EL NUMERO DE CEDULA " + buscar + " NO EXISTE");
                }
                br.close();
                //SE RECORRE EL VECTOR Y SE GUARDAN LAS LINEAS EN EL FICHERO
                if (linea.contains(buscar)) {
                    BufferedWriter bw = new BufferedWriter(new FileWriter(Archivo));
                    for (int i = 0; i < lineasAcopiar.size(); i++) {//Recorrer el vextor
                        linea = (String) lineasAcopiar.elementAt(i);//Se guarda en linea cada string del vector
                        bw.write(linea);//Se escribe la linea en el fichero
                    }
                    bw.close();
                    System.out.println("EL REGISTRO CON NUMERO DE CEDULA " + buscar + " HA SIDO ELIMINADO");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void MostrarPacientes(File Archivo) {
        try {//No tiene parametros porque no necesita informacion previa par apoder continuar y mostrar los registros
            String linea;
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(Archivo));
            while ((linea = br.readLine()) != null) {
                //StringTokenizer ayuda a dividir strigns en base a otro string"caracter" o delimitador 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                while (mistokens.hasMoreTokens()) {//Si depsues de almacenar datos siguen habiendo mas los sigue almacenando
                    String tamRegistro = mistokens.nextToken();
                    //Va leyendo todos los datos
                    String NOMBRE = mistokens.nextToken();
                    String TELEFONO = mistokens.nextToken();
                    String CEDULA = mistokens.nextToken();
                    String DIRECCION = mistokens.nextToken();
                    String FECHA = mistokens.nextToken();
                    String HORA = mistokens.nextToken();
                    String AREA = mistokens.nextToken();
                    //Imprime los datos encontrados en el Archivo de texto de las citas agendadas
                    System.out.println("El nombre es:\t\t" + NOMBRE);
                    System.out.println("El teléfono es:\t\t" + TELEFONO);
                    System.out.println("La cédula es:\t\t" + CEDULA);
                    System.out.println("La dirección es:\t" + DIRECCION);
                    System.out.println("La fecha es:\t\t" + FECHA);
                    System.out.println("La hora es:\t\t" + HORA);
                    System.out.println("El área es:\t\t" + AREA);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
