package proyecto3erciclocitas;

import java.util.*;//para el escaner lectura y entrada en consola
import java.io.*;//Para trabajar los archivos, define distintos flujos de datos

public class PersonalMedico extends Personal {
    //static File ArchivoTXT = new File("DOCTORES.txt");
   int i =1;
    private String ESPECIALIDAD;
    private String GRADOACADEMICO;
    public PersonalMedico() {
    }
    
    public PersonalMedico(String NOMBRE, String TELEFONO, String CEDULA, String ESPECIALIDAD, String GRADOACADEMICO) {
        super(NOMBRE, TELEFONO, CEDULA);
        this.ESPECIALIDAD = ESPECIALIDAD;
        this.GRADOACADEMICO = GRADOACADEMICO;
    }
    public String getESPECIALIDAD() {
        return ESPECIALIDAD;
    }
    public void setESPECIALIDAD(String ESPECIALIDAD) {
        this.ESPECIALIDAD = ESPECIALIDAD;
    }
    public String getGRADOACADEMICO() {
        return GRADOACADEMICO;
    }
    public void setGRADOACADEMICO(String GRADOACADEMICO) {
        this.GRADOACADEMICO = GRADOACADEMICO;
    }
    // String sueldo;
    
    @Override
    public ArrayList<Double> sueldo() {
        final String[] ESPECIALIDADES={"ODONTOLOGA","GINECOLOGA","OFTALMOLOGO","DERMATOLOGO" }; 
        ArrayList<Double> sueldos = new ArrayList<>();
        Archivo objArchivo = new Archivo();
        sueldos = objArchivo.SueldoMedicos();
        System.out.println("▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ SUELDO MÉDICOS ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀");
        for (int j = 0; j < sueldos.size(); j++) {
            System.out.println("SUELDO DE "+ESPECIALIDADES[i]+" ES: "+ sueldos.get(j));
        }   
        return sueldos;
       
    }
}
